﻿using EscuelaNet.Aplicacion.Programadores.QueryModels;
using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Models
{
    public class NuevoProgramadorModel
    {
        public int IdProgramadores { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Legajo { get; set; }
        public string Dni { get; set; }
        public string Rol { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public EstadoDeDisponibilidad Disponibilidad { get; set; }
        public string Conocimiento { get; set; }
        public List<SkillQueryModel> Skills { get; set; }
        public int IdEquipo { get; set; }
    }
}