﻿using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class TecnologiasController : Controller
    {
        private ITecnologiaRepository _tecnoRepository;
        public TecnologiasController(ITecnologiaRepository tecnoRepository)
        {
            _tecnoRepository = tecnoRepository;
        }
        // GET: Tecnologias
        public ActionResult Index()
        {
            var Tecno = _tecnoRepository.ListTecnologias();
            var model = new TecnologiasIndexModel()
            {
                Nombre = "Prueba",
                Tecnologias = Tecno
            };
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevaTecnologiaModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaTecnologiaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    _tecnoRepository.Add(new Tecnologias(model.Nombre));
                    _tecnoRepository.UnitOfWork.SaveChanges();   //Aqui el error
                    TempData["success"] = "Tecnología creada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Campo nombre vacío";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            var Tecno = _tecnoRepository.GetTecnologia(id);
            var model = new NuevaTecnologiaModel()
            {
                Nombre = Tecno.nombre,
                ID = id

            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(NuevaTecnologiaModel model)
        {
            if(!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var tecno = _tecnoRepository.GetTecnologia(model.ID);
                    tecno.nombre = model.Nombre;
                    _tecnoRepository.Update(tecno);
                    _tecnoRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "Tecnología editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Campo nombre vacío";
                    return View(model);
            }
        }

        public ActionResult Delete(int id)
        {

            var Tecno = _tecnoRepository.GetTecnologia(id);
            var model = new NuevaTecnologiaModel()
            {
                Nombre = Tecno.nombre,
                ID = id
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Delete(NuevaTecnologiaModel model)
        {
            try
            {
                var tecno = _tecnoRepository.GetTecnologia(model.ID);
                _tecnoRepository.Delete(tecno);
                _tecnoRepository.UnitOfWork.SaveChanges();
                TempData["success"] = "Tecnología eliminada";
                return RedirectToAction("Index");
            }catch(Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}