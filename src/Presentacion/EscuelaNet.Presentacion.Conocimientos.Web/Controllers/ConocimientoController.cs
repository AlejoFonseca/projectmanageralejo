﻿//using EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura;
using EscuelaNet.Presentacion.Conocimientos.Web.Models;
using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Infraestructura.Conocimientos;
using EscuelaNet.Infraestructura.Conocimientos.Repositorios;
using MediatR;
using EscuelaNet.Aplicacion.Conocimiento.QueryServices;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Conocimiento.Command.ConocimientoCommand;
using EscuelaNet.Aplicacion.Conocimiento.Command;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Controllers
{
    public class ConocimientoController : Controller
    {
        private IMediator _mediator;
        private ICategoriaQuery _categoriaquery;
        private IConocimientoQuery _conocimientoquery;
        private IAsesorQuery _asesorquery;

        public ConocimientoController(IMediator mediator, ICategoriaQuery categoriaquery, IConocimientoQuery conocimientoquery, IAsesorQuery asesorquery) {
            _mediator = mediator;
            _categoriaquery = categoriaquery;
            _conocimientoquery = conocimientoquery;
            _asesorquery = asesorquery;
        }

        
        public ActionResult Index(int id)
        {
            var conocimientos = _conocimientoquery.ListConocimiento(id);
            if (conocimientos.Count() != 0)
            {
                var model = new ConocimientoIndexModel()
                {
                    Titulo = "Conocimientos de la Categoria: '" + conocimientos[0].Nombre + "'",
                    Conocimientos = conocimientos,
                    IdCategoria = id
                };

                return View(model);
            }
            else
            {
                TempData["error"] = "¡ERROR! La categoria solicitada no contiene conocimientos.";
                return RedirectToAction("../Categoria/Index");
            }
            //var categoria = _categoriaRepository.GetCategoria(id);
            //if (categoria.Conocimientos == null)
            //{
            //    TempData["error"] = "Categoria sin Conocimientos";
            //    return RedirectToAction("../Categoria/Index");
            //}
            //var model = new CategoriaIndexModel()
            //{
            //    Titulo = "Conocimientos de la Categoría: '" + categoria.Nombre + "'",
            //    Conocimientos = categoria.Conocimientos.ToList(),
            //    IdCategoria = id
            //};
            //return View(model);
        }

        public ActionResult New(int id)
        {
            var categoria = _categoriaquery.GetCategoria(id);
            var model = new NuevaConocimientoModel()
            {
                Titulo = "Nuevo Conocimiento para la Categoria: '" + categoria.Nombre + "'",
                IdCate = id
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> New(NewConocimientoCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Conocimiento creada";
                return RedirectToAction("Index/" + model.IdCate);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaConocimientoModel()
                {
                    Nombre = model.Nombre,
                    Demanda = model.Demanda,
                    
                    IdCate = model.IdCate,
                };
                return View(modelReturn);
            }
            //if (!string.IsNullOrEmpty(model.Nombre))
            //{
            //    try
            //    {
            //        var nombre = model.Nombre;
            //        var demanda = model.Demanda;

            //        var categoria = _categoriaRepository.GetCategoria(model.IdCate);

            //        var conocimiento = new Conocimiento(nombre);
            //        categoria.AgregarConocimiento(conocimiento);
            //        _categoriaRepository.Update(categoria);
            //        _categoriaRepository.UnitOfWork.SaveChanges();
            //        TempData["success"] = "¡Unidad creada!";
            //        return RedirectToAction("Index/" + model.IdCate);

            //    }
            //    catch (Exception ex)
            //    {
            //        TempData["error"] = ex.Message;
            //        return View(model);
            //    }

            //}
            //else
            //{
            //    TempData["error"] = "Texto vacio, complete los campos solicitados";
            //    return View(model);
            //}
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">el id de la categoria</param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            var conocimiento = _conocimientoquery.GetConocimiento(id);
            if (conocimiento == null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index", new { conocimiento.IDCategoria });
            }
            else
            {
                var model = new NuevaConocimientoModel()
                {
                    Titulo = "Editar el Conocimiento '" + conocimiento.Nombre + "' de la Categoria '" + conocimiento.NombreCategoria + "'",
                    IdCate = conocimiento.IDCategoria,
                    IdConocimiento = id,
                    Nombre = conocimiento.Nombre,
                    Demanda = conocimiento.Demanda
                };
                return View(model);
            }
            //if (id <= 0)
            //{
            //    TempData["error"] = "Id no valido";
            //    return RedirectToAction("Index");
            //}
            //else
            //{
            //    var categoria = _categoriaRepository.GetConocimieto(id);

            //    var model = new NuevaConocimientoModel()
            //    {
            //        Titulo = "Editar el Conocimiento: '" + categoria.Nombre + "' de la Categoría: '" + categoria.Categoria.Nombre + "'",
            //        IdCate = categoria.Categoria.ID,
            //        IdConocimiento = id,
            //        Nombre = categoria.Nombre,
            //        Demanda = categoria.Demanda,
            //    };

            //    return View(model);
            //}
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditConocimientoCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "¡Conocimiento editado correctamente!";
                return RedirectToAction("Index/" + model.IdConocimiento);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaConocimientoModel()
                {
                    Nombre = model.Nombre,
                    Demanda = model.Demanda,
                    IdCate = model.IdConocimiento,
                    IdConocimiento = model.IdConocimiento
                };
                return View(modelReturn);
            }
            //if (!string.IsNullOrEmpty(model.Nombre))

            //{
            //    try
            //    {
            //        var categoria = _categoriaRepository.GetCategoria(model.IdCate);
            //        categoria.Conocimientos.Where(un => un.ID == model.IdConocimiento).First().Nombre = model.Nombre;
            //        categoria.Conocimientos.Where(un => un.ID == model.IdConocimiento).First().Demanda = model.Demanda;

            //        _categoriaRepository.Update(categoria);
            //        _categoriaRepository.UnitOfWork.SaveChanges();
            //        TempData["success"] = "¡Conocimiento editado!";
            //        return RedirectToAction("Index/" + model.IdCate);
            //    }
            //    catch (Exception ex)
            //    {
            //        TempData["error"] = ex.Message;
            //        return View(model);
            //    }

        //}
        //    else
        //    {
        //        TempData["error"] = "Texto vacio";
        //        return View(model);
        //    }
        }

        public ActionResult Delete(int id)
        {
            var conocimiento = _conocimientoquery.GetConocimiento(id);
            if (conocimiento == null)
            {
                TempData["error"] = "¡ERROR! Id no valido";
                return RedirectToAction("../Categoria/Index");
            }
            else
            {
                var model = new NuevaConocimientoModel()
                {
                    Titulo = "Borrar el Conocimiento '" + conocimiento.Nombre + "' de la Categoria: '" + conocimiento.NombreCategoria + "'",
                    IdCate = conocimiento.IDCategoria,
                    IdConocimiento = id,
                    Nombre = conocimiento.Nombre,
                    Demanda = conocimiento.Demanda
                };

                return View(model);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteConocimientoCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Conocimiento borrado correctamente";
                return RedirectToAction("Index/" + model.IdCategoria);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaConocimientoModel()
                {
                    Nombre = model.Nombre,
                    Demanda = model.Demanda,
                    
                    IdCate = model.IdCategoria,
                    IdConocimiento = model.IdConocimiento
                };
                return View(modelReturn);
            }
        }
        //public ActionResult Delete(int id)
        //{
        //    var conocimiento = _conocimientoquery.GetConocimiento(id);
        //    if (conocimiento == null)
        //    {
        //        TempData["error"] = "Id no valido";
        //        return RedirectToAction("../Categoria/Index");
        //    }
        //    else
        //    {
        //        var model = new NuevaConocimientoModel()
        //        {
        //            Titulo = "Borrar la Conocimiento '" + conocimiento.Nombre,
        //            IdCate = conocimiento.IdCategoria,
        //            IdConocimiento = id,
        //            Nombre = conocimiento.Nombre,
        //            Demanda = conocimiento.Demanda,
        //        };

        //        return View(model);
        //if (id < 0)
        //{
        //    TempData["error"] = "Id no valido";
        //    return RedirectToAction("Index");
        //}
        //else
        //{
        //    var conocimiento = _categoriaRepository.GetConocimieto(id);
        //    var model = new NuevaConocimientoModel()
        //    {
        //        Titulo = "Borrar el conocimiento: '" + conocimiento.Nombre + "' de la Categoria: '" + conocimiento.Categoria.Nombre + "'",
        //        IdCate = conocimiento.Categoria.ID,
        //        IdConocimiento = id,
        //        Nombre = conocimiento.Nombre,
        //        Demanda = conocimiento.Demanda,
        //    };
        //    return View(model);
        //}

        //try
        //{
        //    var conocimiento = _categoriaRepository.GetConocimieto(model.IdCate);
        //    _categoriaRepository.DeleteConocimiento(conocimiento);
        //    _categoriaRepository.UnitOfWork.SaveChanges();

        //    TempData["success"] = "¡Conocimiento borrado!";
        //    return RedirectToAction("../Conocimiento/Index/" + model.IdCate);
        //}
        //catch (Exception ex)
        //{
        //    TempData["error"] = ex.Message;
        //    return View(model);
        //}


        public ActionResult Asesor(int id)
        {

            var conocimiento = _conocimientoquery.GetConocimiento(id);
            var asesor = _conocimientoquery.ListAsesorConocimiento(id);
            var model = new AsesorConocimientoModel()
            {
                Conocimiento = conocimiento,
                // Aqui tiene que traer asesores
                //Asesores = asesor
            };
            return View(model);
        }
            //var conocimiento = _categoriaRepository.GetConocimieto(id);
            //var model = new AsesorConocimientoModel()
            //{
            //    Conocimiento = conocimiento,
            //    Asesores = conocimiento.Asesores.ToList()
            //};
            //return View(model);
        
    }
}


