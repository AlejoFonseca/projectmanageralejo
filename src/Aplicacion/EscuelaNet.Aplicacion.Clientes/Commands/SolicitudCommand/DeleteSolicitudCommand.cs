﻿using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.Commands.SolicitudCommand
{
    public class DeleteSolicitudCommand : IRequest<CommandRespond>
    {
        public int IdSolicitud { get; set; }

        public string Titulo { get; set; }

        public string Descripcion { get; set; }

        public EstadoSolicitud Estado { get; set; }

    }
   
}
