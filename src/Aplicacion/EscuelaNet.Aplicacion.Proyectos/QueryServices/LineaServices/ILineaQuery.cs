﻿using EscuelaNet.Aplicacion.Proyectos.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Proyectos.QueryServices.LineaServices
{
    public interface ILineaQuery
    {
        LineaQueryModel GetLinea(int id);
        List<LineaQueryModel> ListLinea();
    }
}
