﻿using EscuelaNet.Aplicacion.Conocimiento.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.QueryServices
{
    public interface ICategoriaQuery
    {
        CategoriaQueryModel GetCategoria(int id);
        List<CategoriaQueryModel> ListCategoria();
    }
}
