﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Conocimiento.QueryModels;

namespace EscuelaNet.Aplicacion.Conocimiento.QueryServices
{
    public class ConocimientoQuery : IConocimientoQuery
    {
        private string _connectionString;
        public ConocimientoQuery(string connectionString)
        {
            _connectionString = connectionString;
        }

        public AsesorQueryModel EncontrarAsesor(int id, int conocimiento)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<AsesorQueryModel>(
                     @"
                    SELECT s.IDAsesor as ID, 
                    s.Nombre as Nombre,
                    s.Apellido as Apellido,
                    s.Disponibilidad as Disponibilidad
					s.Idioma as Idioma,
					s.Pais as Pais
					FROM Conocimiento AS co
                    Inner JOIN AsesorConocimiento as ac ON (ac.conocimiento_ID = co.conocimiento_ID)
					Inner JOIN Asesor as s ON (s.IDAsesor=ac.IDAsesor)
                    WHERE co.conocimiento_ID = @conocimiento AND s.IDAsesor=@id                                    
                    ", new { id = id, conocimiento = conocimiento }
                    ).FirstOrDefault();
                    
            }
        }

        public ConocimientoQueryModel GetConocimiento(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<ConocimientoQueryModel>(
                    @"
                     SELECT c.conocimiento_ID as ID, 
                    c.Nombre as Nombre,
                    c.Demanda as Demanda, c.IDCategoria as IDCategoria
                    FROM Conocimiento as c 
                    INNER JOIN Categoria as ca on (ca.IDCategoria=c.IDCategoria)  
                    WHERE c.conocimiento_ID = @id                                   
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<ConocimientoQueryModel> ListAsesorConocimiento(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<ConocimientoQueryModel>(
                    @"
                   SELECT a.IDAsesor as ID, 
                    a.Nombre as Nombre,
                    a.Apellido as Apellido,
                    a.Disponibilidad as Disponibilidad,
					a.Idioma as Idoma,
					a.Pais as Pais,
					FROM Conocimiento AS co
                    Inner JOIN AsesorConocimiento as ac ON (ac.conocimiento_ID = co.conocimiento_ID)
					Inner JOIN Asesor as a ON (a.IDAsesor=ac.IDAsesor)
                    WHERE co.conocimiento_ID = @id
                    ", new { id = id }
                    ).ToList();
            }
        }

        public List<ConocimientoQueryModel> ListConocimiento(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<ConocimientoQueryModel>(
                     @"SELECT c.conocimiento_ID as ID,
                    c.Nombre as Nombre,
                    c.Demanda as Demanda

                    FROM Conocimiento as c
                    INNER JOIN Categoria as ca on(ca.IDCategoria = c.IDCategoria)
                    WHERE c.IDCategoria = @id",
                    new { id = id }
                    ).ToList();
            }        
        }

        public List<ConocimientoQueryModel> ListTodasConocimiento()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<ConocimientoQueryModel>(
                    @"
                    SELECT c.conocimiento_ID as ID, 
                    c.Nombre as Nombre,
                    c.Demanda as Demanda,
                    FROM Conocimiento as c 
                    INNER JOIN Categorias as ca on (ca.IDCategoria=c.IDCategoria)        
                    "
                    ).ToList();

            }
        }
    }
}
