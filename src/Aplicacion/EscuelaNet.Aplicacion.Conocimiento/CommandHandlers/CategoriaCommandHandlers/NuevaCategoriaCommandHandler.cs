﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Conocimiento.Commands.CategoriaCommand;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandsHandlers.CategoriaCommandHandler
{
    public class NuevaCategoriaCommandHandler: IRequestHandler<NuevaCategoriaCommand, CommandRespond>
    {
        private ICategoriaRepository _categoriaRepositorio;
        public NuevaCategoriaCommandHandler(ICategoriaRepository temaRepositorio)
        {
            _categoriaRepositorio = temaRepositorio;
        }
        public Task<CommandRespond> Handle(NuevaCategoriaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {
                    _categoriaRepositorio.Add(new Categoria(request.Nombre, request.Descripcion));
                    _categoriaRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde); //Preguntar 
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "El nombre está vacio.";
                return Task.FromResult(responde);
            }
        }

        
    }
}
