﻿using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryServices.InstructoresServices
{
    public interface IInstructoresQuery
    {
        InstructorQueryModel GetInstructor(int id);
        List<InstructorQueryModel> ListInstructores();
    }
}
