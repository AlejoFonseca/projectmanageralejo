﻿using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryServices
{
    public interface ILugaresQuery
    {
        LugarQueryModel GetLugar(int id);
        List<LugarQueryModel> ListLugares();
    }
}
