﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.InstructoresHandlers
{
    public class UpdateInstructorCommandHandler : IRequestHandler<UpdateInstructorCommand, CommandRespond>
    {
        private IInstructorRepository _instructorRepository;

        public UpdateInstructorCommandHandler(IInstructorRepository instructorRepository)
        {
            _instructorRepository = instructorRepository;
        }
        public Task<CommandRespond> Handle(UpdateInstructorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (string.IsNullOrEmpty(request.Nombre)
               || string.IsNullOrEmpty(request.Apellido)
               || string.IsNullOrEmpty(request.Dni))
            {

                responde.Succes = false;
                responde.Error = "Campos Vacios";
                return Task.FromResult(responde);
            }
            else
            {
                    var instructor = _instructorRepository.GetInstructor(request.Id);
                    instructor.Nombre = request.Nombre;
                    instructor.Apellido = request.Apellido;
                    instructor.Dni = request.Dni;
                    instructor.FechaNacimiento = request.FechaNacimiento;
                    
                    _instructorRepository.Update(instructor);
                    _instructorRepository.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);

            }
        }
    }
}
