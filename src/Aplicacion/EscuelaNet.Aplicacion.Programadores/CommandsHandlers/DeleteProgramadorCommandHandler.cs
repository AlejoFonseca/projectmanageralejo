﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.CommandsHandlers
{
    public class DeleteProgramadorCommandHandler : IRequestHandler<DeleteProgramadorCommand, CommandRespond>
    {
        private IProgramadorRepository _programadorRepositorio;
        public DeleteProgramadorCommandHandler(IProgramadorRepository programadorRepositorio)
        {
            _programadorRepositorio = programadorRepositorio;
        }
        public Task<CommandRespond> Handle(DeleteProgramadorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            
                try
                {
                    var programador = _programadorRepositorio.GetProgramador(request.Id);
                    _programadorRepositorio.Delete(programador);
                    _programadorRepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);

                }

        }
    }
}
