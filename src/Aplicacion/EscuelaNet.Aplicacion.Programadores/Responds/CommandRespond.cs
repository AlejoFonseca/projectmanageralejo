﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.Responds
{
    public class CommandRespond
    {
        public bool Succes { get; set; }
        public string Error { get; set; }
    }
}
