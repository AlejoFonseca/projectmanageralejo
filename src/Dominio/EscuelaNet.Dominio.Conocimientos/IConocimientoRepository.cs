﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public interface IConocimientoRepository : IRepository<Conocimiento>
    {
        Conocimiento Add(Conocimiento conocimiento);
        void Update(Conocimiento conocimiento);
        void Delete(Conocimiento conocimiento);
        Conocimiento GetConocimiento(int id);
        List<Conocimiento> ListConocimiento();
    }
}
