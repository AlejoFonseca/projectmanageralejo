﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public interface ITecnologiaRepository : IRepository<Tecnologias>
    {
        Tecnologias Add(Tecnologias tecnologias);
        void Update(Tecnologias tecnologias);
        void Delete(Tecnologias tecnologias);       
        Tecnologias GetTecnologia(int id);
        List<Tecnologias> ListTecnologias();
    }
}
