﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public class Tecnologias : Entity, IAggregateRoot
    {
        public string nombre { get; set; }
        public List<Etapa> Etapas { get; set; }

        private Tecnologias()
        {

        }

        public Tecnologias(string nombre)
        {
            this.nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
        }
       
    }
}
